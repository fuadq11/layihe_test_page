//////////////////////////////////////////////////////////////////////////
// * Stopwatch class {{{

////////////////////////////////////////////////////////////////////////


var allquestions = $(".question");
var totalQuestions = 4;

function showOnlyQuestion(number) {

	//Stop all the stopwatches.
	//console.log("show question : ", number);
	thisquestion = $(".question[data-questionnumber='" + number + "']");
	allquestions.hide();
	allquestions.removeClass("active");
	thisquestion.show();
	thisquestion.addClass("active");
	$('.bottombar-highlight').removeClass('bottombar-highlight');
	$(".bottombar-questions[data-questionnumber='" + number + "']").addClass('bottombar-highlight');

	//Start/resume the appropriate watch and set its listener to update
	//every second
	updateButtons();
}

function initialiseQuestions() {

		firstquestion = $(".question").first().data("questionnumber");
		showOnlyQuestion(firstquestion);

}
///////////////////////////////////////////////////////////////////////////


function nextQuestion() {

	var activeQuestion = $(".question.active").data("questionnumber");
	var newQuestion = activeQuestion + 1;
	if (newQuestion <= totalQuestions) { showOnlyQuestion(newQuestion); };

}

function previousQuestion() {
	var activeQuestion = $(".question.active").data("questionnumber");
	var newQuestion = activeQuestion - 1;
	if (newQuestion >= 1) { showOnlyQuestion(newQuestion); }
}

function updateButtons() {
	var activeQuestion = $(".question.active").data("questionnumber");
	$(".nextButton").show();
	$(".previousButton").hide();
	$(".fakeSubmitButton").hide();

	if (activeQuestion == totalQuestions) {

		$(".nextButton").hide();

		$(".fakeSubmitButton").show();

	}

	if (activeQuestion == 1) {
		$(".previousButton").hide();
	}
	else {
		$(".previousButton").show();
	}

}

$(window).resize(function () {

	if (34 * $('.bottombar-questions').length > $(window).width() - 2 * $('.nextButton').width()) {
		$('.pagination').width($(window).width() - 2 * $('.nextButton').width() - 2 * $('.backscroll').width());
	} else {
		$('.forwardscroll').hide();
		$('.backscroll').hide();
	}
});

var autoSave = null;
/////////////////////////////////////////////////////////////////////////
$(document).ready(function () {

    showOnlyQuestion(2);
	if (34 * $('.bottombar-questions').length > $(window).width() - 2 * $('.nextButton').width()) {
		$('.pagination').width($(window).width() - 2 * $('.nextButton').width() - 2 * $('.backscroll').width());
	} else {
		$('.forwardscroll').hide();
		$('.backscroll').hide();
	};

	$('body').on('click touchstart tap', '.forwardscroll', function () {
		$('ul').animate({
			scrollLeft: '+=150'
		}, 350);
	});

	$('body').on('click touchstart tap', '.backscroll', function () {
		$('ul').animate({
			scrollLeft: '-=150'
		}, 350);
	});



	$('body').on('click touchstart tap', '.question_answer', function () {
		var qname = $(this).attr('data-question-id');
		var value = $(this).attr('data-answer-value');
		$('input[type="radio"][name="' + qname + '"][value="' + value + '"]').prop('checked', true);
		$(this).prop('checked', false);
		$('.selected-answer[data-question-id="' + qname + '"]').removeClass('selected-answer');
		$(this).addClass('selected-answer');

		numberOfQuestionsAttempted++;
	});

	$('body').keydown(function (event) {
		if (event.which == 37) {
			event.preventDefault();
			$(".previousButton").click();
		}
		if (event.which == 39) {
			event.preventDefault();
			$('.nextButton').click();
		}
	});

	$(".nextButton").click(function (e) {
		nextQuestion();
		e.preventDefault();
	});

	$(".previousButton").click(function (e) {
		previousQuestion();
		e.preventDefault();
	});

	$('.btn-submit-completed').click(function () {
		$('.submitButton').click();
	});

	$(".fakeSubmitButton").click(function (e) {

		// Check to see if there are any blank questions
		// Check time condition

		var elapsedMin = globalWatch.getElapsed().hours * 60 + globalWatch.getElapsed().minutes;
		var timeCondition = (examDurationMins - elapsedMin) > 1;
		var questionCondition = numberOfQuestionsAttempted - $(".question").length < 0;

		if (questionCondition || timeCondition) {
			$('#exit-dialog').modal('show');
		}
		else {
			$('.submitButton').click();
		}

	});

	$(".solutionButton").click(function (e) {
		$(this).parent().parent().find(".solution").slideToggle();
		e.preventDefault();
	});

	function getQuestionsData() {
		var questions = [];
		$(".question").each(function () {

			// Make an object for each question
			// This should have the question ID, the user Answer and the time taken.

			questionObj = {};

			var question_id = $(this).data('questionid');
			var user_answer = $(this).find("input:checked").val();
			var time_taken = $(this).find(".timer").text().trim();

			if (user_answer == undefined) { var user_answer = "0"; }

			questionObj['question_id'] = question_id;
			questionObj['user_answer'] = user_answer;
			questionObj['time_taken'] = time_taken;

			if ($(this).hasClass("active")) {
				questionObj['active'] = true;
			}

			questions.push(questionObj);
		});

		return questions;

	}

	var submitted = 0;
	//console.log(submitted);
	if (submitted == 0) {
		$("#practice_form").on('submit', function (e) {

			var form = this;
			e.preventDefault();
			$("body").addClass("loading");

			getUpdatedFormData();

			submitted = 1;
			//console.log(submitted);

			form.submit();
		});

	}

});
var time_left= 100*60;
// Update the count down every 1 second
var x = setInterval(function() {

	time_left--;
  var minutes= Math.floor(time_left / 60);
	var second = Math.floor(time_left % 60);

  // Output the result in an element with id="demo"
  document.getElementById("global_watch").innerHTML = minutes +":" +second;

  // If the count down is over, write some text
  if (time_left < 0) {
    clearInterval(x);
    document.getElementById("global_watch").innerHTML = "EXPIRED";
  }
}, 1000);




/////////////////////////////////////////////////////////////////////////
